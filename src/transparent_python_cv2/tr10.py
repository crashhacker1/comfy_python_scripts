def remove_bkg(self, img):
        # quick sloppy background removal######
        self.log.info("removing background")
        #use grabcutwith facerecthas foreground
        mask = np.zeros(img.shape[:2], np.uint8)
        bgdModel = np.zeros((1, 65), np.float64)
        fgdModel = np.zeros((1, 65), np.float64)
        mask2 = np.where((mask == 2) | (mask == 0), 0, 1).astype('uint8')
        cv2.grabCut(img, mask, self.rect, bgdModel, fgdModel, 5, cv2.GC_INIT_WITH_RECT)
        img = img * mask2[:, :, np.newaxis]
        return img

    # vision context
